.. _utiliser_tests:


#####################################
Créer ses tests
#####################################

Dans cette partie, nous allons voir comment mettre en place un système de tests automatisés permettant de vérifier le bon fonctionnement de notre application. Le lancement de ces tests servira à :

* Initialiser la base de données
* Ajouter des emetteurs, services et courriers lors de l'initialisation
* Saisir automatiquement dans le navigateur, à l'aide de sélénium, un emetteur, un service et un courrier
* Vérifier qu'il n'est pas possible de supprimer un service et un emetteur liés à un courrier


=====================================
Installation  et paramètrage
=====================================

Commencer par vérifier que le répertoire *tests* existe. L'arborescence de ce répertoire est décrite ci-après: :ref:`arborescence_repertoire_tests`.

Pour l'installation, dans **Tests et Integration Continue**, suivre la partie :ref:`installation_tests`.

Aller ensuite dans *tests/binary_files/dyn/database.inc.php* et vérifier que le paramétrage d'accés à la base de données est correct, si ce n'est pas le cas le corriger.


=====================================
Initialisation de la base de données
=====================================

Dans un premier temps, on va initialiser l'environnement de test. Pour cela, il faut que l'environnement de test soit activé((om-tests) present devant la ligne dans le terminal) et lancer la commande suivante depuis le repertoire tests : ::

	om-tests -c initenv

Après initialisation, on peut constater que les tables service, emetteur et courrier n'existe plus dans la base de données. En effet, jusqu'à present ces tables ont été ajoutées à l'aide de postgres depuis le terminal, elles ne sont donc pas initilaiser dans le code. Pour remedier à cela, il faut ajouter, dans *data/pgsql/install.sql*, avant le commit, le code suivant : ::

	-- Création des séquences

	CREATE SEQUENCE emetteur_seq
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;
	
	CREATE SEQUENCE service_seq
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;
	
	CREATE SEQUENCE courrier_seq
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE 1;
	
	-- Création des tables
	
	CREATE TABLE emetteur (
	    emetteur          int PRIMARY KEY,  -- clé primaire
	    nom               varchar(20),
	    prenom            varchar(20)
	);
	
	CREATE TABLE service (
	    service           int PRIMARY KEY,  -- clé primaire
	    libelle           varchar(20)
	);
	
	CREATE TABLE courrier (
	    courrier          int PRIMARY KEY,           -- clé primaire
	    dateenvoi         date,
	    objetcourrier     text,
	    registre	      varchar(20),
	    emetteur          int REFERENCES emetteur,   -- clé étrangère
	    service           int REFERENCES service     -- clé étrangère
	);

On va ensuite, remplir de manière automatique ces tables lors de l'initialisation. Dans le fichier *tests/data/pgsql/install_tests.sql* inserer, avant le commit, les lignes suivantes : ::

	-- insertion de deux émetteurs avec récupération et incrémentation de la table de séquences
	
	INSERT INTO emetteur (emetteur, nom, prenom) VALUES
	(nextval('emetteur_seq'), 'dupont', 'pierre'),
	(nextval('emetteur_seq'), 'durant', 'jacques');
	
	-- insertion de deux services avec récupération et incrémentation de la table de séquences
	
	INSERT INTO service (service, libelle) VALUES
	(nextval('service_seq'), 'informatique'),
	(nextval('service_seq'), 'telephonie');
	
	-- insertion de deux courriers avec récupération et incrémentation de la table de séquences
	
	INSERT INTO courrier (courrier, dateenvoi, objetcourrier, emetteur, service) VALUES
	(nextval('courrier_seq'), '2010-12-01', 'Proposition de fourniture de service', 1, 1),
	(nextval('courrier_seq'), '2010-12-02', 'Envoi de devis pour formation openMairie', 2, 1);
	
Relancer la commande initenv et verifier que les tables sont créées et remplies. 


===================================================
Simuler l'ajout d'un nouvel objet par l'utilisateur
===================================================

On cherche maintenant à ecrire un test permettant de simuler la creation d'un service, d'un emetteur et d'un courrier par l'utilisateur.

Dans le repertoire *tests* créer un nouveau fichier qu'on appelera **012_mytest.robot** et ajouter les lignes suivantes : ::

	*** Settings ***
	Resource  resources/resources.robot
	Suite Setup  For Suite Setup
	Suite Teardown  For Suite Teardown
	Documentation  TestSuite "Mytest"...
	
	*** Test Cases ***


On s'occupe ensuite de faire un test simulant l'ajout d'un service par l'utilisateur. En premier lieu, inserer à la suite du **Test Cases** le code suivant :  :: 

	Ajouter un service

	    [Documentation]  On teste l'ajout d'un service 
	    ...  Le service ajouté :
	    ...  'comptabilité'
	
	    #connection à l'application
	    Depuis la page d'accueil  admin  admin
	    #On accéde à l'écran des services
	    Go To Submenu In Menu  parametrage  service
	    #On véerifie le titre de la page
	    Page Title Should Be  Application > Service
	    #On vérifie que le menu est ouvert sur l'element correct
	    Submenu In Menu Should Be Selected  parametrage  service
	    #On clique sur le bouton ajouter
	    Click On Add Button
	    #On ajoute l'element comptabilite
	    Input Text	css=#libelle	comptabilité
	    #On valide le formulaire
	    Click On Submit Button
	    #On vérifie que la page ne contiens pas d'erreur
	    La page ne doit pas contenir d'erreur


On peut ensuite lancer ce test avec la commande : ::

	om-tests -c runone -t 012_mytest.robot

Les résultats de ces tests sont visibles dans *tests/results/log.html* et *tests/results/report.html*

Vous devriez voir une fenetre firefox s'ouvrir sur l'application et réaliser une à une les étapes du test. 

**Remarque** : Le test simule, à l'aide de selenium, les actions réalisées par un utilisateur sur le navigateur. Par conséquent, si vous touchez au navigateur pendant que les tests s'effectuent cela aura un impact sur leur déroulement. Il ne faut donc pas toucher à la fenêtre pendant qu'un test est lancé.

Pour éviter d'être dérangé par le navigateur pendant que les tests sont lancés, on peut également lancer les commandes suivantes :

*pour un seul tests*  ::

	xvfb-run -a --server-args=-screen\ O\ 1680*1030*24 om-test -c runone -t 012_mytest.robot

*pour lancer tous les tests* ::

	xvfb-run -a --server-args=-screen\ O\ 1680*1030*24 om-test -c runall

Ajouter, ensuite un emetteur : ::

	Ajouter un emetteur

	    [Documentation]  On teste l'ajout d'un emetteur 
	    ...  Emetteur ajouté :
	    ...  nom : test, prenom : jean
	
	    #connection à l'application
	    Depuis la page d'accueil  admin  admin
	    #On accéde à l'écran des services
	    Go To Submenu In Menu  parametrage  emetteur
	    #On véerifie le titre de la page
	    Page Title Should Be  Application > Emetteur
	    #On vérifie que le menu est ouvert sur l'element correct
	    Submenu In Menu Should Be Selected  parametrage  emetteur
	    #On clique sur le bouton ajouter
	    Click On Add Button
	    #On entre le nom et le prenom de l'emetteur
	    Input Text	css=#nom	test
	    Input Text	css=#prenom	jean
	    #On valide le formulaire
	    Click On Submit Button
	    #On vérifie que la page ne contiens pas d'erreur
	    La page ne doit pas contenir d'erreur


Puis un courrier : ::

	Ajouter un courrier

	    [Documentation]  On teste l'ajout d'un courrier
	    ...  courrier ajouté :
	    ...  date : 09/06/2020, objet : mangez des kiwis
	    ...  emetteur : test, service : comptabilité
	
	    #connection à l'application
	    Depuis la page d'accueil  admin  admin
	    #On accéde à l'écran des services
	    Go To Submenu In Menu  parametrage  courrier
	    #On véerifie le titre de la page
	    Page Title Should Be  Application > Courrier
	    #On vérifie que le menu est ouvert sur l'element correct
	    Submenu In Menu Should Be Selected  parametrage  courrier
	    #On clique sur le bouton ajouter
	    Click On Add Button
	    #On remplit les champs du courrier
	    Input Datepicker  dateenvoi  09/06/2020
	    Input Text	css=#objetcourrier	mangez des kiwis
	    Select From List By Label	css=#emetteur	test
	    Select From List By Label	css=#service	comptabilité
	    #On valide le formulaire
	    Click On Submit Button
	    #On vérifie que la page ne contiens pas d'erreur
	    La page ne doit pas contenir d'erreur

==========================================
Tester la suppression d'un élément
==========================================

On cherche maintenant à vérifier que l'utilisateur ne peut pas supprimer un emetteur et/ou un service lié à un élément de la table courrier. Pour cela, on va se servir des mots clés générés automatiquement à partir du système de données. 

Ajouter dans la partie **Settings** : ::

	#à la suite de Resource
	Resource  resources/app/gen/service.robot


Ajouter ensuite le test suivant : ::

	Suppression d'un service lié à la table courrier

	    [Documentation]  On teste la suppression d'un service
	    ...  service supprimé :
	    ...  comptabilité
	
	    #connection à l'application
	    Depuis la page d'accueil  admin  admin
	    #On accéde à l'écran des services
	    Go To Submenu In Menu  parametrage  service
	    #On véerifie le titre de la page
	    Page Title Should Be  Application > Service
	    #On vérifie que le menu est ouvert sur l'element correct
	    Submenu In Menu Should Be Selected  parametrage  service
	    #On supprime le service comptabilite
	    Supprimer service 	3
	    #On verifie le message d'erreur
	    Error Message Should Contain  SUPPRESSION NON EFFECTUÉE

**Remarque** : le mot clé "Supprimer service" est généré automatiquement à partir du modèle de données, dans le fichier *resources/app/gen/service.robot*

De la même manière, vous pouvez écrire le test de suppression d'un emetteur et d'un courrier.



