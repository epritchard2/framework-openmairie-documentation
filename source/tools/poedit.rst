######
POEdit
######

POEdit est un éditeur de traductions de chaînes en plusieurs langues.
Les chaînes présentes dans l'interface des applications openMairie sont celles
présentes dans le code.

Les étapes sont les suivantes :

1 - avoir dans le code .php les chaînes à traduire, comme argument de la fonction ``gettext()`` (alias ``_()`` )

2 - préparer les dossiers de traductions dans le dossier de locales

3 - avoir préalablement installé et configuré POEdit

4 - configurer le projet dans POEdit et effectuer un scan du code afin de détecter
    les chaînes à traduire

5 - traduire les chaînes dans POEdit et sauvegarder

6 - à l'exécution du PHP la chaine est traduite, un redémarrage du serveur web peut être 
    nécessaire


=================================================
Spécification dans le code des chaînes à traduire
=================================================

Les chaînes peuvent être traduites, soit en français, soit dans
d'autres langues. Pour cela il est nécessaire qu'elles soient présentes dans
les fichiers.php en respectant la syntaxe suivante ::

``__('Ma chaine à traduire')``

Toutes les chaînes de caractères correspondant aux noms de tables et de champs
sont générées par le générateur et sont ainsi directement disponibles pour une
traduction.

Depuis OM 4.9, la fonction ``__()`` surcharge la fonction ``_()``. Cette fonction cherche 
à traduire en utilisant:

* d'abord le domaine de traduction `applicatif`: ``./locales/fr_FR/LC_MESSAGES/openmairie.po``
* sinon le domaine de traduction `framework`: ``./core/locales/fr_FR/LC_MESSAGES/framework-openmairie.po``
* sinon en renvoyant un second argument communicable par la fonction ``__($msg_id, $msg_default)``
* sinon le texte est renvoyé en l'état

(exemple pour l'environnement système ``fr_FR``)


===================================
Préparation des dossiers de locales
===================================

Chaque application openMairie comporte, à la racine, un dossier appelé
``locales``.
Ce dossier comporte une structure de type ::

    locales/
        fr_FR/
            LC_MESSAGES/
                openmairie.po
                openmairie.mo

Il est nécessaire de créer un dossier de langue (par exemple ``en_US``) avec
son sous-dossier ``LC_MESSAGES`` pour chaque langue supplémentaire.

Le fichier .po contient les définitions de traductions ; il peut être modifié
au moyen de POEdit ou directement depuis un éditeur de texte simple.

Le fichier .mo contient les traductions sous une forme compilée. Il est généré
par POEdit automatiquement lors de chaque sauvegarde des traductions.


=======================================
Installation et configuration de POEdit
=======================================

Installation
------------

POEdit est disponible nativement dans la plupart des systèmes linux. Il est
possible de le télécharger depuis le site officiel pour tous Linux, Windows et
Mac OSX.

http://www.poedit.net/download.php

Sous Linux Ubuntu ou Debian, il faut, en root, exécuter la commande
``apt-get install poedit``

Gestion de plusieurs langues
----------------------------

Une fois installé, il faut s'assurer que les ``locales`` (fichiers de définition
de langues) sont correctement installés sur le système.

Sous Linux Ubuntu, pour ajouter une locale, il faut ajouter sa définition dans
le fichier de pays correspondant (exemple : ``/var/lib/locales/supported.d/fr``)
puis lancer la commande, en root, ``dpkg-reconfigure locales``. Cela ne sera
nécessaire que pour ajouter la prise en charge d'une nouvelle langue.

=====================================
Configuration d'un projet dans POEdit
=====================================

- Ouvrir le projet en ouvrant le fichier ``./locales/fr_FR/LC_MESSAGES/openmairie.po``

- Configurer le projet: menu ``Catalogue > Propriétés ...``

  + Propriétés de traduction
  
    * Nom et version du projet: ``openmonapplication``
    * Equipe de langue: Nom contact <contact@monorganisation.org>``
    * Langue: ``français`` pour fr_FR
    * Jeux de caractètre: UTF-8
  + Chemin des sources
    
    * Chemin de base: le répertoire racine de l'application ``./`` ... le chemin sera peut-être affiché en absolu, mais sera bien stocké en relatif par POEdit
    * Chemins: ``.`` ... ceci permet d'inclure par défaut tout répertoire ajouté à vos sources 
    * Chemins exclus: les chemins pris en charge par le domaine de traduction du framework
    
      - ``core``
      - ``gen/obj/om*``
      - ``gen/sql/pgsql/om*``
      - ``lib``
      - ``php``
  + Mots-clés sources
  
    * Ajouter ``__`` pour pouvoir utiliser la fonction ``__()`` en plus de ``_()``
    * Cocher ``Utiliser également les mots-clés par défaut pour les langues supportées``
    
- Cliquer alors sur le bouton 'Mettre à jour tous les catalogues du projet' :
  un scan de code est alors effectué par POEdit. Cela va parcourir tous les
  fichiers .php du dossier afin de détecter toutes les chaînes encadrées par
  ``__()`` et ``_()``.
  
  Le scan peut comporter des erreurs (des chaînes à traduire vides etc.).
  Dans ce cas le fichier et la ligne concernée sont indiqués dans
  le rapport d'erreur. Il est recommandé de corriger l'erreur et recommencer
  la mise à jour.
  
  Le rapport affiche les chaînes désuètes (celles qui ne figurent plus dans le
  code) et les nouvelles chaînes. Les chaînes désuètes sont alors commentées
  dans le fichier .po et n'apparaissent plus dans l'interface de traduction.

  Pour les chaînes relevant du framework openMairie, mais importée dans les
  sources lors d'une surcharge, on recommande de laisser intraduit. On peut 
  ajouter un commentaire pour guider les prochains traducteurs.
  Pour les chaines qu'on souhaitent avoir traduites à l'identique, mieux vaut
  indiquer une traduction identique pour mieux identifier les chaines intraduites.
  

======================
Traduction des chaînes
======================

Depuis l'écran du projet dans POEdit, double-cliquer sur le fichier de la langue
concernée. La liste des chaînes à traduire apparaît alors.
Les nouvelles chaînes sont en premier, les chaînes modifiées en second et les
autres chaînes ensuite. Il suffit de cliquer sur une chaîne et entrer la
traduction dans le bloc de texte inférieur.

Il y a des raccourcis pour gagner du temps: 

* ``CTRL+B``: copier le texte-source dans la traduction
* ``CTRL+BAS``: passer au texte-source suivant
* ``click-droit`` indique les fichiers source concernés

A chaque sauvegarde, le fichier est compilé en un fichier ``.mo``, l'application 
affiche alors les chaînes traduites à la place des libellés originaux.

Attention, sur certaines configurations, un redémarrage du serveur web peut
être nécessaire pour que les traductions soient mises à jour.

